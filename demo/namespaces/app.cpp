#include <iostream>
#include <string>
#include "functions.hxx"

namespace {
    using namespace std;
    string greeting() {
        return "Hello from the APP module";
    }
}

int main() {
    std::cout << greeting() << std::endl;
    std::cout << ribomation::factorial(5) << std::endl;
    return 0;
}


