#include <iostream>
#include <string>
#include "functions.hxx"

namespace {
    using namespace std;

    string greeting() {
        return "Hello from the FUNCTIONS module";
    }

    unsigned long factorialUsingRecursion(unsigned n) {
        return n <= 1 ? 1 : n * factorialUsingRecursion(n - 1);
    }
}

namespace ribomation {
    unsigned long factorial(unsigned n) {
        using namespace std;
        cout << greeting() << endl;
        return factorialUsingRecursion(n);
    }
}


