cmake_minimum_required(VERSION 3.5)
project(async_exceptions)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmax-errors=1 -Wall -Wextra ")

add_executable(catch-nullptr
        Trace.hxx
        AsyncException.hxx
        catch-nullptr.cxx
        )
