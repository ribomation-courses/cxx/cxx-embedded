#include <iostream>
using namespace std;

template<typename T>
T mulBy2(T n) {
    cout << __PRETTY_FUNCTION__ << ": ";
    return n * 2;
}

template<>
int mulBy2(int n) {
    cout << __PRETTY_FUNCTION__ << " SPZ: ";
    return n << 1;
}


int main(int, char**) {
    cout << "3.1415 = " << mulBy2(3.1415) << endl;
    cout << "21     = " << mulBy2(21) << endl;
    return 0;
}
