#include <iostream>
#include <type_traits>

using namespace std;

template<typename T>
T maximum(T a, T b) { return a >= b ? a : b; }

template<typename T1, typename T2>
typename common_type<T1, T2>::type
MAXIMUM(T1 a, T2 b) { return a >= b ? a : b; }


int main(int, char**) {
    int   x = 10, y = 42;
    float z = 21.5;

    cout << "max(x,y) = " << maximum(x, y) << endl;

    //cout << "max(x,z) = " << maximum(x, z) << endl;
    //error: no matching function for call to ‘maximum(int&, float&)’

    cout << "MAX(x,y) = " << MAXIMUM(x, y) << endl;
    cout << "MAX(x,z) = " << MAXIMUM(x, z) << endl;

    return 0;
}

