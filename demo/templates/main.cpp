#include <iostream>
#include <string>

using namespace std;
using namespace string_literals;

template<typename T>
T abs(T x) { return x < 0 ? -x : x; }

void using_abs() {
    int    r1 = abs<int>(-42);
    double r2 = abs<double>(-42);
    char   r3 = abs<char>(-42);
    int    r4 = abs<>(-42);
    int    r5 = abs(-42);

    cout << "r1:" << r1 << endl;
    cout << "r2:" << r2 << endl;
    cout << "r3:" << r3 << endl;
    cout << "r4:" << r4 << endl;
    cout << "r5:" << r5 << endl;
}


template<short N, long M, typename T>
T mul(T x) { return N * M * x; };

int main() {
    cout << "mul = " << mul<3, 7>(2) << endl;
    cout << "mul = " << mul<3, 7>(3.1415) << endl;
    return 0;
}
