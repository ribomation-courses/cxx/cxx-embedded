#include <iostream>
using namespace std;

struct Account {
    const string accno;
    const int balance;
    Account(const string& a, int b) : accno{a}, balance{b} {}
    friend ostream& operator<<(ostream& os, const Account& a){
        return os << "Account{" << a.accno << ", " << a.balance << "}";
    }
};

template<typename T>
class Node {
    T       payload;
    Node*   next;
public:
    Node(T payload, Node<T>* next = nullptr)
            : payload{payload}, next{next} {}
    T       data() const { return payload; }
    Node*   link() const { return next; }
};

int main(int, char**) {
    Node<Account>* first = nullptr;
    first = new Node<Account>{Account{"1234-8888"s, 4200}, first};
    first = new Node<Account>{Account{"4321-3333"s, 2100}, first};
    for (Node<Account>* n = first; n != nullptr; n = n->link())
        cout << "acc: " << n->data() << endl;
    return 0;
}

