#include <iostream>

using namespace std;

template<int N, typename T>
T mul(T x) {
    static_assert(N >= 2);
    return x * N;
};

void using_mul() {
    cout << "mul = " << mul<2>(21) << endl;
    //cout << "mul = " << mul<1>(42) << endl;
}

#include <type_traits>

template <typename T>
T mulBy2(T x) {
    static_assert(is_integral<T>::value,
                  "Param type must be integral, i.e. short/int/long");
    return x << 1;
}

int main(int, char**) {
    cout << "mulBy2 = " << mulBy2(21) << endl;
    //cout << "mulBy2 = " << mulBy2(21.0) << endl;
    return 0;
}


