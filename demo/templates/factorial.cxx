#include <iostream>

using namespace std;

using UL = unsigned long;

template<unsigned N>
struct Factorial {
    constexpr static UL value = N * Factorial<N - 1>::value;
};

template<>
struct Factorial<1> {
    constexpr static UL value = 1;
};

int main(int, char**) {
    UL values[] = {
            Factorial<1>::value,
            Factorial<5>::value,
            Factorial<10>::value,
            Factorial<12>::value,
            Factorial<20>::value,
    };
    for (auto x : values) cout << x << endl;
    return 0;
}


