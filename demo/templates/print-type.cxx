#include <cstdio>
#include <string>

template<typename T>
void printType(T) {puts(__PRETTY_FUNCTION__);}

int main(int, char**) {
    using namespace std::string_literals;
    printType(42);
    printType(3.1415926);
    printType('A');
    printType("Hello");
    printType("Hello"s);
    printType<std::string>("Hello");
    return 0;
}
