#include <iostream>

using namespace std;

template<typename T=int, unsigned N=12>
class Stack {
    T        stk[N];
    unsigned top = 0;
public:
    void    push(T x) { stk[top++] = x; }
    T       pop() { return stk[--top]; }
    bool    empty() const { return top == 0; }
    bool    full() const { return top >= N; }
};

int main(int, char**) {
    Stack<> s;

    for (auto k = 1; !s.full(); ++k) s.push(k);
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;

    return 0;
}


