#include <iostream>
#include <string>
#include <type_traits>
#include <cassert>
#include <cmath>

using namespace std;
using namespace std::string_literals;


template<typename T>
string toString(T value, true_type) {
    return to_string(value);
}

template<typename T>
string toString(T value, false_type) {
    return "??";  //No known string conversion of type
}


template<typename T>
string asString(T value) {
    return toString(value, is_arithmetic<T>());
}

template<>
string asString(bool value) {
    return value ? "TRUE" : "FALSE";
}

template<>
string asString(string value) {
    return value;
}


template<typename Head>
string csv(Head head) {
    return asString(head);
}

template<typename Head, typename... Tail>
string csv(Head head, Tail... tail) {
    return asString(head) + ";"s + csv(tail...);
};


int main(int, char**) {
    auto result = csv("Anna Conda"s, 42, 2 * asin(1), sizeof(int) > 1);
    cout << "CSV: '" << result << "'" << endl;
    return 0;
}

