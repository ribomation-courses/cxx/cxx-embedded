#include <iostream>
#include <memory>

using namespace std;

struct Thing {
    static int count;
    const int  value;

    explicit Thing(int v) : value{v} {
        ++count;
        cout << "Thing(int=" << value << ") cnt=" << count << endl;
    }

    ~Thing() {
        --count;
        cout << "~Thing(" << value << ") cnt=" << count << endl;
    }

    Thing(const Thing&) = delete;
    Thing& operator=(const Thing&) = delete;
};
int Thing::count = 0;


unique_ptr<Thing> mk() {
    unique_ptr<Thing> ptr{new Thing{42}};
    cout << "[mk]    count: " << Thing::count << endl;
    return ptr;
}

unique_ptr<Thing> use(unique_ptr<Thing> ptr) {
    cout << "[use]   count: " << Thing::count << endl;

    auto q = make_unique<Thing>(17);
    cout << "[use]   count: " << Thing::count << endl;
    ptr = move(q);

    return ptr;
}

int main(int, char**) {
    cout << "[first] count: " << Thing::count << endl;
    {
        unique_ptr<Thing> ptr = mk();
        ptr = use(move(ptr));
        cout << "[block] count: " << Thing::count << endl;
    }
    cout << "[last]  count: " << Thing::count << endl;
    return 0;
}


