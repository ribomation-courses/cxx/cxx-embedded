#include <iostream>
#include <memory>

using namespace std;


int main(int, char**) {
    cout << "[main] enter\n";
    {
        struct Deleter {
            void operator()(int* addr) {
                cout << "custom deleter\n"; delete addr;
            }
        };
        unique_ptr<int, Deleter> p{new int{42}, Deleter{}};
        cout << "*p = " << *p << endl;
    }
    cout << "[main] exit\n";
    return 0;
}

