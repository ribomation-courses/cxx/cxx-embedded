#include <iostream>
#include <string>
#include <functional>
#include <tuple>
#include <chrono>

using namespace std;
using namespace std::chrono;

template<typename RetType, typename ArgType>
tuple<RetType, long>
elapsed(function<RetType(ArgType)> f, ArgType arg) {
    auto    start  = high_resolution_clock::now();
    RetType result = f(arg);
    auto    end    = high_resolution_clock::now();
    return make_tuple(result, duration_cast<nanoseconds>(end - start).count());
}

long SUM(int n) { return n * (n + 1) / 2; }

int main() {
    auto N         = 10000;
    auto [sum, ns] = elapsed<long, int>(SUM, N);
    cout << "sum[1.." << N << "] = " << sum << " (elapsed " << ns << " ns)" << endl;
    return 0;
}

