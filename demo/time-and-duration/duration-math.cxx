#include <iostream>
#include <chrono>

using namespace std;
using namespace chrono;
using namespace chrono_literals;

int main(int, char**) {
    auto weekend_run = 1h + 12min + 17s + 125ms;
    cout << "last w/e run took " << weekend_run.count() << " seconds" << endl;

    cout << "which is "
         << duration_cast<hours>(weekend_run).count()                     << "h + "
         << duration_cast<minutes>(weekend_run % hours(1)).count()        << "m + "
         << duration_cast<seconds>(weekend_run % minutes(1)).count()      << "s + "
         << duration_cast<milliseconds>(weekend_run % seconds(1)).count() << "ms"
         << endl;

    return 0;
}


