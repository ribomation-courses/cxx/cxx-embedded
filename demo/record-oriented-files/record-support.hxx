#pragma once

#include <cstring>
#include <string>

namespace ribomation {
namespace records {
using namespace std;

struct RecordSupport {
  virtual void toExternal(char *storage) = 0;
  virtual void fromExternal(char *storage) = 0;

  void *toStorage(string &variable, unsigned numBytes, void *offset) {
    ::memset(offset, '\0', numBytes);
    variable.copy(static_cast<char *>(offset), numBytes, 0);
    return offset + numBytes;
  }

  template <typename NumericType>
  void *toStorage(NumericType value, void *offset) {
    ::memcpy(offset, &value, sizeof(NumericType));
    return offset + sizeof(NumericType);
  }

  void *fromStorage(string &variable, unsigned numBytes, void *offset) {
    string buf(static_cast<char *>(offset), numBytes);
    variable = trim(buf);
    return offset + numBytes;
  }

  template <typename NumericType>
  void *fromStorage(NumericType &variable, void *offset) {
    ::memcpy(&variable, offset, sizeof(NumericType));
    return offset + sizeof(NumericType);
  }

private:
  string trim(const string &txt) {
    string result;
    for (unsigned k = 0; k < txt.size(); ++k) {
      if (txt[k])
        result += txt[k];
    }
    return result;
  }
};
}
}
