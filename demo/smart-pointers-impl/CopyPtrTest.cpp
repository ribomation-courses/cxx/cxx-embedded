#include <iostream>
#include <string>
#include "CopyPtr.h"
#include "trace.h"
#include "Person.h"
using namespace std;

int  InstanceCount::count = 0;

CopyPtr<Person>    compute(CopyPtr<Person> ptr) {
	Trace t("compute");
	cout << "[compute] ptr = " << *ptr << endl;
	cout << "[compute] #persons = " << Person::numObjs() << endl;
	return ptr;
}

int main() {
	Trace t("main");
	cout << "#persons = " << Person::numObjs() << endl;
	
	{
		Trace b("block-1");
		CopyPtr<int>  p(new int(17));	
		cout << "p = " << *p << endl;
		*p = *p + 10;
		cout << "p = " << *p << endl;
	}
	
	{
		Trace b("block-2");
		CopyPtr<Person> nisse(new Person("nisse"));
		cout << "nisse = " << *nisse << endl;
		cout << "#persons = " << Person::numObjs() << endl;
		
		cout << "*** Copying of 'nisse'" << endl;
		CopyPtr<Person>  q(nisse);
		cout << "#persons = " << Person::numObjs() << endl;
		try {
			cout << "nisse = " << *nisse << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (nisse) !!!" << endl;}
		try {
			cout << "q = " << *q << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (q) !!!" << endl;}
		
		cout << "*** Parameter passing to and from compute()" << endl;
		nisse = compute(q);
		cout << "#persons = " << Person::numObjs() << endl;
		try {
			cout << "nisse = " << *nisse << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (nisse) !!!" << endl;}
		try {
			cout << "q = " << *q << endl;
		} catch (NullPointerException) {cout << "Got NullPointer (q) !!!" << endl;}
		
		cout << "*** Before end of inner block (destructor call)" << endl;
	}

	cout << "#persons = " << Person::numObjs() << endl;
	return 0;
}
