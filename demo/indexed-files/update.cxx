#include <iostream>
#include <iomanip>
#include <string>
#include <random>
#include <algorithm>
#include <numeric>
#include "account.hxx"
#include "db/indexed-file.hxx"

using namespace std;
using namespace std::literals;
using namespace bank;
using namespace ribomation::io;


double totalBalance(IndexedFile<Account>& file) {
    return accumulate(file.begin(), file.end(), 0, [](auto sum, auto acc) {
        return sum + acc.getBalance();
    });
}

int main(int argc, char** argv) {
    auto filename = "accounts.db"s;

    for (auto k                  = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-f") filename = argv[++k];
    }

    random_device              r;
    normal_distribution<float> amount(0, 100);
    IndexedFile<Account>       file{filename, OpenMode::readwrite};
    const auto                 N = file.count();

    cout.imbue(locale{"en_US.UTF8"});
    cout << "Initial: SEK " << fixed << setprecision(2) << totalBalance(file) << endl;
    auto      totalAmount = 0.0;
    for (auto k           = 0U; k < N; ++k) {
        Account acc = file[k];
        auto    amt = amount(r);
        acc += amt;
        totalAmount += amt;
        file[k] = acc;
    }
    cout << "Amount : SEK " << totalAmount << " (" << N << " accounts)" << endl;
    cout << "Final  : SEK " << fixed << setprecision(2) << totalBalance(file) << endl;

    return 0;
}
