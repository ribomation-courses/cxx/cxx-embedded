#include <iostream>
#include <iomanip>
#include <bitset>
#include <cstdint>

using namespace std;

using Port = uint8_t;

using Board = struct {
    Port port0;
    Port port1;
};

static Board fakeBoard;


void dump() {
    cout << "FakeBoard: |"
         << bitset<4>(fakeBoard.port0)
         << "|  |"
         << bitset<4>(fakeBoard.port1)
         << "|"
         << endl;
}


int main() {
    dump();

    return 0;
}
