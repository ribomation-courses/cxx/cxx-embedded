#include <iostream>
#include <string>
#include <optional>
using namespace std;

optional<string> lookup(const string& haystack, const string& phrase) {
    auto pos = haystack.find(phrase);
    if (pos != string::npos) return {haystack.substr(pos, phrase.size())}; else return {};
}

void doit(const string& phrase){
    const auto sentence = "It was a dark and silent night. Suddenly there was a sound of a shot."s;
    cout << "find("<<phrase<<"): " << lookup(sentence, phrase).value_or("NOT FOUND") << endl;
}

int main(int, char**) {
    doit("shot");
    doit("what");
    return 0;
}


