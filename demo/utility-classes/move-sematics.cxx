#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <locale>

using namespace std;

struct Thing {
    static int count;
    explicit Thing(int) { ++count; }
    Thing(const Thing&) { ++count; }
    ~Thing() { --count; }
};
int Thing::count = 0;

vector<Thing> alloc(int n) {
    vector<Thing> words;
    while (--n >= 0) { words.emplace_back(n); }
    cout << "[alloc] count: " << Thing::count << endl;
    return words;
}


int main(int, char**) {
    cout.imbue(locale(""));
    cout << "[first] count: " << Thing::count << endl;
    {
        vector<Thing> result = alloc(1'000'000);
        cout << "[inner] size : " << fixed << result.size() << endl;
        cout << "[inner] count: " << Thing::count << endl;
    }
    cout << "[last]  count: " << Thing::count << endl;
    return 0;
}
