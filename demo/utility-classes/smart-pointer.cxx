#include <iostream>
#include <memory>

using namespace std;

struct Thing {
    static int count;
    explicit Thing(int) { ++count; }
    Thing(const Thing&) { ++count; }
    ~Thing() { --count; }
};
int Thing::count = 0;

unique_ptr<Thing> mk() {
    unique_ptr<Thing> ptr{new Thing{42}};
    cout << "[mk]    count: " << Thing::count << endl;
    return ptr;
}

unique_ptr<Thing> use(unique_ptr<Thing> ptr) {
    cout << "[use]   count: " << Thing::count << endl;
    return ptr;
}

int main(int, char**) {
    cout << "[first] count: " << Thing::count << endl;
    {
        unique_ptr<Thing> ptr = mk();
        ptr = use(move(ptr));
        cout << "[block] count: " << Thing::count << endl;
    }
    cout << "[last]  count: " << Thing::count << endl;
    return 0;
}


