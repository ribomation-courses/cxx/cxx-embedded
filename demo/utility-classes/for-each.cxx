#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <set>

using namespace std;

void for_each_over_int_vector() {
    vector<int> numbers = {1, 2, 3, 5, 8, 13, 21};
    for (auto   n : numbers) cout << n << " ";
    cout << endl;
}

void for_each_over_string_set() {
    set<string> words = {"to", "language", "use", "C++ is a", "cool"};
    for (auto& w : words) cout << w << " ";
    cout << endl;
}

void for_each_over_bounded_array() {
    double    rates[] = {0.015, 0.025, 0.035, 0.075, 0.125, 0.085};
    for (auto r : rates) cout <<fixed<< setprecision(2) << r * 100 << "% ";
    cout << endl;
}


int main(int, char**) {
    for_each_over_int_vector();
    for_each_over_string_set();
    for_each_over_bounded_array();
    return 0;
}
