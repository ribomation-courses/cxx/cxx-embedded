#include <iostream>
#include <string>

using namespace std;

void magic(int& v) {
    cout << "magic(int&) : " << v << endl;
    ++v;
}

void magic(int&& v) {
    cout << "magic(int&&): " << v << endl;
    ++v;
}

int main(int, char**) {
    int value = 10;

    magic(value);
    magic(value + 15);
    magic(5 + 15);
    magic(move(value));

    return 0;
}


