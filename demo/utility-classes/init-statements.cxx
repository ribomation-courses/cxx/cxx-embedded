#include <iostream>
#include <string>

using namespace std;

void searchFor(const string& needle, const string& haystack) {
    if (const auto found = haystack.find(needle); found != string::npos) {
        cout << "yup, found it here at pos " << found << endl;
    } else {
        cout << "nah, " << needle << " is not here " << endl;
    }
}

int main(int, char**) {
    const string sentence = "Just a silly sentence with a lot of dummy words";
    searchFor("silly", sentence);
    searchFor("xyz", sentence);
    return 0;
}


