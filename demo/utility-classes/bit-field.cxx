#include <iostream>
#include <cassert>

using namespace std;

typedef struct {
    unsigned char op:3;
    unsigned char flags:2;
    unsigned char padding:3;
} Data;

int main(int, char**) {
    unsigned d = 0b011'10'101;
    auto* pkg = (Data*) &d;

    cout << "op=" << unsigned(pkg->op)
         << ", flags=" << unsigned(pkg->flags)
         << ", padding=" << unsigned(pkg->padding)
         << endl;
    assert(pkg->op == 5);
    assert(pkg->flags == 2);
    assert(pkg->padding == 3);

    return 0;
}


