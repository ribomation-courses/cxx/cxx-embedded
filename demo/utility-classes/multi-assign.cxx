#include <iostream>
#include <string>
#include <tuple>
#include <map>

using namespace std;

tuple<string, unsigned, float>
mkPerson() {
    return make_tuple("Justin Time"s, 42U, 63.7F);
};

void multi_assign_from_tuple() {
    auto[name, age, weight] = mkPerson();
    cout << "name  : " << name << endl;
    cout << "age   : " << age << endl;
    cout << "weight: " << weight << endl;
}

void multi_assign_from_map_iteration() {
    map<string, string> words = {
            {"one",   "ett"},
            {"two",   "två"},
            {"three", "tre"},
            {"four",  "fyra"},
            {"five",  "fem"},
    };
    for (auto& [en, sw] : words)
        cout << en << " -> " << sw << endl;
}

int main() {
    multi_assign_from_tuple();
    cout << "----\n";
    multi_assign_from_map_iteration();
    return 0;
}

