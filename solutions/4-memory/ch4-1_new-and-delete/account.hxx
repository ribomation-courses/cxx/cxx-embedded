#pragma once

#include <iosfwd>
#include <vector>

namespace ribomation::types {
    using namespace std;

    class Account {
        int   balance = 0;
        float rate    = 0.75;

        static vector<Account> storage;
        static vector<bool>    used;

    public:
        Account(int b, float r) : balance{b}, rate{r} {}

        ~Account() = default;
        Account() = default;
        Account(const Account&) = default;
        Account& operator =(const Account&) = default;

        void* operator new(size_t);
        void operator delete(void* ptr);

        string toString() const;

        friend ostream& operator <<(ostream& os, const Account& acc) {
            return os << acc.toString();
        }

        static auto capacity() { return storage.size(); }
    };

}
