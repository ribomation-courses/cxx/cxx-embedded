#include <iostream>
#include <iomanip>
#include "length.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::length;

int main() {
    auto len = 2.25_m + 1.5_km - 0.15_mi - 3.25_ya;
    cout << "distance: " << fixed << setprecision(3) << len << endl;

    cout << "answer: " << fixed << setprecision(0) << 42.0_m << endl;
    //cout << "answer-2: " << 42_m << endl;

    return 0;
}
