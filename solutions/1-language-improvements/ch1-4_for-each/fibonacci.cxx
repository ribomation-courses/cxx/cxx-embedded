#include <iostream>
#include <iomanip>
#include <string>
using namespace std;
using namespace std::literals;

class Fibonacci {
    unsigned n;
public:
    Fibonacci(unsigned n) : n{n} {}
    ~Fibonacci() = default;
    Fibonacci(const Fibonacci&) = default;
    Fibonacci& operator =(const Fibonacci&) = default;

    struct iterator {
        unsigned long f2 = 0;
        unsigned long f1 = 1;
        unsigned      n;

        iterator(unsigned n) : n{n} {}
        bool          operator !=(iterator& rhs) { return n != rhs.n; }
        unsigned long operator *() { return f1; }
        void          operator ++() {
            unsigned long f = f2 + f1;
            f2 = f1;
            f1 = f;
            ++n;
        }
    };

    iterator begin() const { return {0}; }
    iterator end()   const { return {n}; }
};

int main(int argc, char** argv) {
    auto n = argc == 1 ? 92U : stoi(argv[1]);
    for (auto f : Fibonacci{n})
        cout << setw(20) << f << endl;
    return 0;
}
