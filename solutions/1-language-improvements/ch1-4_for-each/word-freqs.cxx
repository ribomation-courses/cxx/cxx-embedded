#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>
using namespace std;

int main() {
    auto filename = "../word-freqs.cxx"s;
    istream&& input = ifstream{filename};

    map<string, unsigned> freqs;
    for (string word; input >> word;) ++freqs[word];

    for (auto& [word, freq] : freqs)
        cout << setw(24) << left << ("\""s + word + "\""s) << ": " << freq << endl;
    return 0;
}
