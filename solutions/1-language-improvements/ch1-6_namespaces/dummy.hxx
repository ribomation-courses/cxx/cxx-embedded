#pragma once

namespace se::ribomation::cxx_course {

    struct Dummy {
        Dummy();
        Dummy(const Dummy&);
        virtual ~Dummy();
    };

}
