#include <iostream>
#include "dummy.hxx"
using namespace std;
using namespace se::ribomation::cxx_course;

namespace {
    auto theAnswer = 42U;
}

void func1(Dummy x) {
    cout << "[func-1] &x = " << &x << endl;
}

void func2(const Dummy& x) {
    cout << "[func-2] &x = " << &x << endl;
}

int main() {
    Dummy d{};
    func1(d);
    func2(d);
    cout << "[main] The Answer is " << theAnswer << endl;
    return 0;
}

