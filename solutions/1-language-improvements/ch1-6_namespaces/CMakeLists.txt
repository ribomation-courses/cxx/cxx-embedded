cmake_minimum_required(VERSION 3.9)
project(3_namespaces)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic -Werror -Wfatal-errors")

add_executable(namespaces
        dummy.hxx dummy.cxx
        app.cxx
        )

