#include <iostream>
#include "dummy.hxx"

namespace {
    auto theAnswer = 42U;
}

namespace se::ribomation::cxx_course {

    Dummy::Dummy() {
        std::cout << "Good-day from a Dummy class @ " << this << std::endl;
        std::cout << "[Dummy] The Answer is " << theAnswer << std::endl;
    }

    Dummy::Dummy(const Dummy&) {
        using std::cout;
        using std::endl;
        cout << "Copy of a Dummy class @ " << this << endl;
    }

    Dummy::~Dummy() {
        using namespace std;
        cout << "Good-bye from a Dummy class @ " << this << endl;
    }

}
