#include <iostream>
#include <iomanip>
#include <cmath>
#include "maclaurin.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::numeric;

int main() {
    auto x      = 0.5 * M_PI;
    auto approx = sinApprox(x, 8U);
    auto result = sin(x);

    cout << "x      = " << fixed << x << endl;
    cout << "approx = " << fixed << approx << endl;
    cout << "result = " << fixed << result << endl;
    cout << "diff   = " << fixed << setprecision(20) << abs(result - approx) << endl;
    return 0;
}
