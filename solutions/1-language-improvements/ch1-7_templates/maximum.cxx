#include <iostream>
#include <string>
using namespace std;
using namespace std::literals;

template<typename T>
T MAXIMUM(T a, T b) { return a >= b ? a : b; }

template<typename T>
T maximum(T a, T b, T c) { return MAXIMUM(MAXIMUM(a,b), c); }

template<typename T, typename ... Args>
T MAXIMUM(T a, T b , Args ...args) {
    return MAXIMUM(MAXIMUM(a,b), args...);
}

int main() {
    cout << "int   : " << maximum(5, 10, 3) << endl;
    cout << "double: " << maximum(5.1, 10.3, 3.9) << endl;
    cout << "string: " << maximum("aa"s, "AA"s, "ZZ"s) << endl;

    cout << "int   : " << MAXIMUM(5, 10, 3, 2, 12, 16, 1, 42) << endl;
    cout << "string: " << MAXIMUM("a"s, "B"s, "c"s, "D"s, "d"s, "F"s, "G"s) << endl;

    return 0;
}

