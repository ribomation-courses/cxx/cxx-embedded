#include <iostream>
#include <sstream>
#include <string>
#include "vector-3d.hxx"

using namespace std;
using namespace std::literals;

void test_non_numeric() {
//    Vector3d<string>  v;
}


int main() {
    Vector3d<> v1{2, 5, 10};
    cout << "v1: " << v1 << endl;

    istringstream{"data: <10 20 30>"} >> v1;
    cout << "v1: " << v1 << endl;

    cout << "v1 * 42: " << v1 * 42 << endl;
    cout << "10 * v1: " << 10 * v1 << endl;

    Vector3d<> v2{2, 2, 2};
    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;

    return 0;
}
