#include <iostream>
#include <tuple>
#include <map>
using namespace std;

auto fibonacci(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

auto compute(unsigned n) {
    return make_tuple(n, fibonacci(n));
}

auto populate(unsigned n) {
    map<unsigned, unsigned> results;
    for (auto k = 1U; k <= n; ++k) {
        auto [arg, fib] = compute(k);
        results[arg] = fib;
    }
    return results;
}

int main() {
    for (auto [arg, fib] : populate(42))
        cout << "fib(" << arg << ") = " << fib << endl;
    return 0;
}

