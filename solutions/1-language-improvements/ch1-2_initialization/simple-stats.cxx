#include <iostream>
#include <initializer_list>
#include <algorithm>
#include <numeric>

using namespace std;

class SimpleStats {
    unsigned _count;
    float    _sum;
    float    _max;
    float    _min;
public:
    SimpleStats(initializer_list<float> args) {
        _count = static_cast<unsigned>(args.size());
        _sum   = accumulate(args.begin(), args.end(), 0);
        _min   = *min_element(args.begin(), args.end());
        _max   = *max_element(args.begin(), args.end());
    }

    unsigned count() const { return _count; }
    float min() const { return _min; }
    float max() const { return _max; }
    float avg() const { return _sum / _count; }

    ~SimpleStats() = default;
    SimpleStats(const SimpleStats&) = default;
    SimpleStats& operator =(const SimpleStats&) = default;

    friend ostream& operator <<(ostream& os, const SimpleStats& s) {
        return os << "cnt: " << s.count()
                  << ", min/max: " << s.min() << "/" << s.max()
                  << ", avg: " << s.avg();
    }
};

int main() {
    SimpleStats s1{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    cout << "s1: " << s1 << endl;
    cout << "s2: " << SimpleStats{55, 55, 55, 55} << endl;
    return 0;
}
