#include <iostream>
#include <string>
#include <set>
using namespace std;
using namespace std::literals;

int main() {
    set<string> words = {
            "language"s, "C++"s, "is a cool"s
    };
    for (set<string>::iterator it = words.begin(); it != words.end(); ++it) {
        string w = *it;
        cout << w << " ";
    }
    cout << endl;
    return 0;
}
