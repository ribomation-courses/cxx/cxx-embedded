#include <iostream>

using namespace std;

class Thing {
    const unsigned value = 42;
public:
    Thing() = default;
    ~Thing() = default;
    Thing(const Thing&) = delete;
    Thing& operator =(const Thing&) = delete;

    friend ostream& operator <<(ostream& os, const Thing& t) {
        return os << "Thing{" << t.value << "}";
    }
};

void func(Thing t) {
    cout << t << endl;
}

void funcr(Thing& t) {
    cout << "funcr: " << t << endl;
}

void funcp(Thing* t) {
    cout << "funcp: " << *t << endl;
}

int main() {
    Thing t1;
    cout << "t1: " << t1 << endl;

    // func(t1);
    // use of deleted function Thing::Thing(const Thing&)

    funcr(t1);
    funcp(&t1);

    Thing t2;
    cout << "t2: " << t2 << endl;

    // t2 = t1;
    // use of deleted function Thing& Thing::operator=(const Thing&)

    return 0;
}
