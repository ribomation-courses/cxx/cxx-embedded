#include <iostream>
#include <functional>

using namespace std;

int reduce(int* arr, unsigned n, function<int(int, int)> f) {
    auto result = arr[0];
    for (auto k = 1U; k < n; ++k) result = f(result, arr[k]);
    return result;
}

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 2, 1};
    const auto N         = sizeof(numbers) / sizeof(numbers[0]);

    cout << "SUM : " << reduce(numbers, N, [](auto sum, auto x) { return sum + x; }) << endl;
    cout << "PROD: " << reduce(numbers, N, [](auto prod, auto x) { return prod * x; }) << endl;
    cout << "MAX : " << reduce(numbers, N, [](auto max, auto x) { return max > x ? max : x; }) << endl;
    return 0;
}
