#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <iterator>
#include <cctype>
using namespace std;
using namespace std::literals;

unordered_multiset<string>
load(istream& in) {
    unordered_multiset<string> words;

    for (string word; in >> word;) {
        string WORD;
        copy_if(word.begin(), word.end(), back_inserter(WORD), [](auto ch) {
            return isalpha(ch);
        });
        if (WORD.size() > 3) {
            transform(WORD.begin(), WORD.end(), WORD.begin(), [](auto ch){return tolower(ch);});
            words.insert(WORD);
        }
    }

    return words;
}

int main(int argc, char** argv) {
    auto     filename = (argc == 1) ? "../musketeers.txt"s : argv[1];
    ifstream file{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto             words = load(file);
    multiset<string> freqs{words.begin(), words.end()};

    for (auto it = freqs.begin(); it != freqs.end();) {
        auto word = *it;
        auto freq = freqs.count(word);
        cout << word << ": " << freq << endl;
        advance(it, freq);
    }
    return 0;
}
