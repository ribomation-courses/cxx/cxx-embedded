#include <iostream>
#include <string>
#include <optional>

using namespace std;
using namespace std::literals;

struct Address {
    optional<string>   street;
    optional<unsigned> postCode;
    optional<string>   city;
};

class Contact {
    string            name;
    optional<string>  email;
    optional<string>  url;
    optional<Address> address;
public:
    Contact(string name) : name{name} {}
    ~Contact() = default;
    Contact(const Contact&) = default;
    Contact& operator =(const Contact&) = default;

    string getName() const { return name; }
    optional<string> getEmail() const { return email; }
    optional<string> getUrl() const { return url; }
    optional<Address> getAddress() const { return address; }

    Contact& setName(string name) {
        Contact::name = name; return *this;
    }
    Contact& setEmail(string email) {
        Contact::email = email; return *this;
    }
    Contact& setUrl(string url) {
        Contact::url = url; return *this;
    }
    Contact& setAddress(string street, unsigned postCode, string city) {
        Contact::address = {street, postCode, city};
        return *this;
    }

    friend ostream& operator <<(ostream& os, const Contact& c) {
        os << "name=" << c.name;
        if (c.email)  //c.email.operator bool()
            os << "\nemail=" << c.email.value();
        if (c.url)
            os << "\nurl=" << c.url.value();
        if (c.address)
            os << "\n"
               << c.address.value().street.value()
               << ", " << c.address.value().postCode.value()
               << ", " << c.address.value().city.value();
        return os;
    }
};

int main() {
    Contact jens = Contact{"Jens Riboe"}
            .setEmail("jens.riboe@ribomation.se"s)
            .setUrl("https://www.ribomation.se/"s)
            .setAddress("Stureplan 4C"s, 11435, "Stockholm"s);

    Contact contacts[] = {
            jens,
            Contact{"Anna Conda"s},
            Contact{"Justin Time"s}.setEmail("justin@foobar.com"s),
            Contact{"Per Silja"s}.setAddress("42 Reboot Lane", 12345, "PWA")
    };
    unsigned id = 1;
    for (auto& c : contacts)
        cout << "Contact " << id++ << ":\n" << c << "\n\n";

    return 0;
}
