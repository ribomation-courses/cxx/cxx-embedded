#!/usr/bin/env bash
set -uex

CC="gcc"
CCXX="g++ -std=c++17"
WARN="-Wall -Wextra -Wfatal-errors"
LDFLAGS="-Wl,--allow-multiple-definition"

${CCXX} ${WARN} -c func.cxx
${CCXX} ${WARN} -c func-app.cxx
rm -f func-lib.a
ar crs func-lib.a func.o func-app.o
${CCXX} func-lib.a -o func-app
./func-app

${CC} ${WARN} -c patched-func.c
${CCXX} ${LDFLAGS} patched-func.o func-lib.a -o patched-func-app
./patched-func-app

