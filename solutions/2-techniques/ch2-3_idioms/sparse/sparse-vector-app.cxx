#include <iostream>
#include "sparse-vector.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::types;

int main() {
    SparseVector<int> v;
    cout << "v: " << v << " (" << v.capacity() << ")" << endl;

    v[5] = 42;
    cout << "v: " << v << " (" << v.capacity() << ")"<< endl;

    v[15] = 4242;
    cout << "v: " << v << " (" << v.capacity() << ")"<< endl;
    new int{666};
    return 0;
}

