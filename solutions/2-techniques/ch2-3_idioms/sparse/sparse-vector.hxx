#pragma once

#include <iosfwd>
#include <map>
#include <stdexcept>
#include <string>

namespace ribomation::types {
    using namespace std;

    template<typename T>
    class SparseVector {
        map<unsigned, T> data;

    public:
        SparseVector() = default;
        ~SparseVector() = default;
        SparseVector(const SparseVector<T>&) = default;
        SparseVector<T>& operator =(const SparseVector<T>&) = default;

        bool     empty()    const { return data.empty(); }
        unsigned capacity() const { return static_cast<unsigned>(data.size()); }
        unsigned maxIndex() const { return data.empty() ? 0 : data.rbegin()->first; }

        struct iterator {
            SparseVector<T>& v;
            unsigned ix;
            iterator(SparseVector<T>& v, unsigned ix) : v{v}, ix{ix} {}

            void operator =(T x) { //WRITE
                v.data[ix] = x;
            }
            operator T() { //READ
                return v.data.count(ix) == 0 ? T{} : v.data[ix];
            }
            bool operator !=(const iterator& rhs) {
                return this->ix != rhs.ix;
            }
            void operator ++() { ++ix; }
            T    operator *() { return operator T(); }
        };

        iterator operator [](unsigned idx) {
            if (1 <= idx) {
                return {*this, idx};
            }
            throw underflow_error{"index "s + to_string(idx) + " out of bounds [1,"s
                                  + to_string(maxIndex()) + "["s};
        }

        iterator begin() { return {*this, 1U}; }
        iterator end()   { return {*this, maxIndex() + 1}; }

        friend ostream& operator <<(ostream& os, SparseVector<T>& v) {
            os << "[";
            bool      first = true;
            for (auto x : v) {
                if (first) first = false; else os << ", ";
                os << x;
            }
            os << "]";
            return os;
        }
    };

}
