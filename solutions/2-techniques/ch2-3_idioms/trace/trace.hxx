#pragma once
#include <iostream>

namespace ribomation::debug {
    using namespace std;
    using namespace std::literals;

    class Trace {
       static ostream* logfile;
        const string name;
        const unsigned long        address;
        bool                       hasReturned = false;

    public:
        Trace(string name, void* addr) : name{name}, address{reinterpret_cast<unsigned long>(addr)} {
            *logfile << "CREATE " << name << " @ " << address << endl;
        }

        Trace(string name) : name{name}, address{0} {
            *logfile << "ENTER " << name << endl;
        }

        Trace(const Trace&) = default;

        ~Trace() {
            if (hasReturned) return;
            if (address == 0) {
                *logfile << "EXIT " << name << endl;
            } else {
                *logfile << "DISPOSE " << name << " @ " << address << endl;
            }
        }

        template<typename T>
        T RETURN(T x) {
            *logfile << "RETURN " << name << " -> " << x << endl;
            hasReturned = true;
            return x;
        }

        static void setOutput(ostream* out) {
            logfile = out;
        }
    };

}
