#pragma once

#include <iosfwd>
#include "trace.hxx"

namespace ribomation::domain {
    using namespace std;
    using namespace std::literals;
    using namespace ribomation::debug;

    class Person {
        Trace        t;
        const string name;
    public:
        Person(string s) : t{"Person"s, this}, name{s} {}

        friend ostream& operator <<(ostream& os, const Person& p) {
            return os << "Person{" << p.name << "}";
        }
    };

}