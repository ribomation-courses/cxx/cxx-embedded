#include <iostream>
#include <string>
#include "trace.hxx"
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::debug;
using namespace ribomation::domain;


long sum(unsigned n) {
    Trace t{"sum"};
    return t.RETURN(n * (n + 1) / 2);
}


int main(int argc, char** argv) {
    Trace t{"main"};
    {
        Trace  t{"inner"};
        Person p{"Nisse"};
        cout << "p: " << p << endl;
    }
    auto  result = sum(10);
    cout << "SUM(1..10) = " << result << endl;
    return 0;
}
