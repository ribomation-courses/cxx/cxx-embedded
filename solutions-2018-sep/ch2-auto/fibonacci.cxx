#include <iostream>
#include <string>
#include <tuple>
#include <map>
using namespace std;
using namespace std::literals;

auto fibonacci(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

auto compute(unsigned n) {
    return make_tuple(n, fibonacci(n));
}

auto populate(unsigned n) {
    map<unsigned, unsigned> values;
    for (auto k = 1U; k <= n; ++k) {
        auto[arg, fib] = compute(k);
        values[arg] = fib;
    }
    return values;
}

int main(int argc, char** argv) {
    auto n      = argc == 1 ? 10U : stoi(argv[1]);
    auto values = populate(n);
    for (auto[arg, fib] : values) {
        cout << "fib(" << arg << ") = " << fib << endl;
    }
    return 0;
}

