#include <iostream>
#include <string>
#include <initializer_list>
#include <numeric>

using namespace std;
using namespace std::literals;

string toCSV(string delim, initializer_list<string> args) {
    return accumulate(args.begin(), args.end(), ""s, [=](string result, string arg) {
        return result + (result.empty() ? ""s : delim) + arg;
    });
}

int main() {
    auto csv = toCSV(";"s, {"Anna Conda"s, "42 Reboot Lane", "CX X17"s, "Core Valley"s, "true"});
    cout << "CSV: " << csv << endl;
    return 0;
}
