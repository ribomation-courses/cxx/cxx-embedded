#include <iostream>

using namespace std;
using namespace std::literals;

int main() {
    cout << "DEC: " << 42U << endl;
    cout << "HEX: " << 0x2A << endl;
    cout << "OCT: " << 052 << endl;
    cout << "BIN: " << 0b0010'1010 << endl;
    return 0;
}
