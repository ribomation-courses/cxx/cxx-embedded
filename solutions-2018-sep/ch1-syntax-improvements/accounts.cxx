#include <iostream>
#include <string>
#include <vector>
using namespace std;
using namespace std::literals;


struct Account {
    int   balance = 0;
    float rate    = 1.5;
    Account(int balance, float rate = 1.75) : balance{balance}, rate{rate} {}
    Account() = default;
    Account(const Account&) = default;
    Account& operator =(const Account&) = default;

    double update() {
        balance = static_cast<int>(balance * (1 + rate / 100.0));
        return balance;
    }

    string toString() const {
        return "Account{SEK "s + to_string(balance) + ", "s + to_string(rate) + "%}"s;
    }

    friend ostream& operator <<(ostream& os, const Account& acc) {
        return os << acc.toString();
    }
};

vector<Account> populate() {
    return {
            {100, 0.25},
            {200, 0.5},
            {300, 0.75},
            {400, 1.0},
            {500, 1.25},
    };
}

int main() {
    vector<Account> accounts = populate();
    for (auto& acc : accounts) cout << acc << endl;
    return 0;
}
