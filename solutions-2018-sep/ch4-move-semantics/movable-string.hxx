#pragma once
#include <iostream>
#include <cstring>

namespace ribomation {
    using namespace std;

    class MovableString {
        using Address = unsigned long;
        const char* payload = nullptr;
        bool        hasPayload()     const { return payload != nullptr; }
        Address     payloadAddress() const { return reinterpret_cast<Address>(payload); }
        char*       clone(const char* s) {
            return strcpy(new char[strlen(s) + 1], s);
        }

    public:
        MovableString() = delete;
        MovableString(const MovableString&) = delete;
        MovableString& operator=(const MovableString&) = delete;

        MovableString(const char* s) : payload{clone(s)} {
            cout << "MovableString(char* '" << payload << "') @ " << this << endl;
        }

        ~MovableString() {
            cout << "~MovableString() @ " << this;
            if (hasPayload()) {
                cout << " DELETE \"" << payload << "\" @ " << payloadAddress();
                delete [] payload;
            } else {
                cout << " no payload";
            }
            cout << endl;
        }

        MovableString(MovableString&& that) : payload{that.payload} {
            that.payload = nullptr;
            cout << "MovableString(&& '" << payload << "') @ " << this << endl;
        }

        MovableString& operator=(MovableString&& that) {
            cout << "MovableString::operator=(&& '" << payload << "') @ " << this;
            if (this != &that) {
                if (hasPayload()) {
                    cout << " DELETE \"" << payload << "\"";
                    delete [] payload;
                }
                payload = that.payload;
                that.payload = nullptr;
            }
            cout << endl;
            return *this;
        }

        MovableString& operator +=(const MovableString& rhs) {
            const size_t  BUF_SIZE = 1 + strlen(this->payload) + strlen(rhs.payload);
            char          buf[BUF_SIZE];
            strcpy(buf, this->payload);
            strcat(buf, rhs.payload);

            delete [] this->payload;
            this->payload = clone(buf);

            return *this;
        }

        friend MovableString operator +(const MovableString& lhs, const MovableString& rhs) {
            const size_t  BUF_SIZE = 1 + strlen(lhs.payload) + strlen(rhs.payload);
            char          buf[BUF_SIZE];
            strcpy(buf, lhs.payload);
            strcat(buf, rhs.payload);
            return {buf};
        }

        friend std::ostream& operator <<(std::ostream& os, const MovableString& s) {
            return os << '"' << (s.hasPayload() ? s.payload : "") << "\" @ " << s.payloadAddress();
        }
    };

}
