#include <string>
#include <vector>
#include "movable-string.hxx"
using namespace std;
using namespace ribomation;

static const char* names[] = {"anna", "berit", "carin", "diana", "eva", "frida"};
static const auto names_size = sizeof(names) / sizeof(names[0]);

MovableString mk() {
    static int next = 0;
    cout << "mk(): " << names[next % names_size] << endl;
    return {names[next++ % names_size]};
}

void useIt(MovableString s) {
    cout << "[use-it] s=" << s << endl;
}

int main() {
    MovableString s1{"Tjabba Habba"};
    cout << "s1: " << s1 << endl;

    cout << "---- (1) ----\n";
    s1 = mk();
    cout << "s1: " << s1 << endl;

    cout << "---- (2) ----\n";
    {
        MovableString s2 = move(s1);
        cout << "s1: " << s1 << endl;
        cout << "s2: " << s2 << endl;

        cout << "---- (2b) ----\n";
        MovableString s3 = s2 + " -- " + mk();
        cout << "s3: " << s3 << endl;
    }

    cout << "---- (3) ----\n";
    {
        MovableString s4{mk()};
        cout << "s4: " << s4 << endl;

        MovableString s5 = mk();
        cout << "s5: " << s5 << endl;
    }

    cout << "---- (4) ----\n";
    {
        MovableString ts = "";
        for (auto     k  = 0U; k < names_size; ++k) {
            ts += (k == 0 ? "" : ", ") + mk();
        }
        cout << "ts: " << ts << endl;
    }

    cout << "---- (5) ----\n";
    {
        MovableString s5 = mk();
        cout << "[before] s5: " << s5 << endl;
        useIt(move(s5));
        cout << "[after] s5: " << s5 << endl;
    }

    //MovableString s4{s3};
    //error: use of deleted function ‘MovableString::MovableString(const MovableString&)’

    //s1 = s3;
    //error: use of deleted function ‘MovableString& MovableString::operator=(const MovableString&)’

    //MovableString s4;
    //error: use of deleted function ‘MovableString::MovableString()’

    cout << "---- (4)\n";
    return 0;
}
