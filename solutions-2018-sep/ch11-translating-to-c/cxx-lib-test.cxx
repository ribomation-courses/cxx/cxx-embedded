#include <iostream>
#include "cxx-lib.hxx"
using namespace std;

int main() {
    auto s = toUpperCase("Foobar strikes again");
    cout << "s: " << s << endl;
    delete s;
    return 0;
}

