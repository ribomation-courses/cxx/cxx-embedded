#pragma once
#include <iostream>

namespace se::ribomation::cxx_course {
    struct Dummy {
        Dummy(int);
        Dummy(const Dummy&);
        ~Dummy();
        int get() const {return value;}
    private:
        const int value;
    };
}
