#include <iostream>
#include "dummy.hxx"

using namespace std;
using namespace se::ribomation::cxx_course;

ostream& operator<<(ostream& os, const Dummy& d) {
    return os << "Dummy{" << d.get() << "} @ " << &d;
}

namespace {
    void byReference(const Dummy& x) {
        cout << "[by-ref] x = " << x << endl;
    }

    void byPointer(const Dummy* x) {
        cout << "[by-ptr] x = " << *x << endl;
    }

    void byValue(Dummy x) {
        cout << "[by-val] x = " << x << endl;
    }
}

int main() {
    cout << "[main] enter\n";
    {
        Dummy d{42};
        byValue(d);
        byReference(d);
        byPointer(&d);
    }
    cout << "[main] exit\n";
    return 0;
}
