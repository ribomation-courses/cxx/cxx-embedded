#include <iostream>
#include "dummy.hxx"

se::ribomation::cxx_course::Dummy::Dummy(int value) : value{value} {
    using std::cout;
    using std::endl;
    cout << "Dummy(" << value << ") @ " << this << endl;
}

se::ribomation::cxx_course::Dummy::Dummy(const se::ribomation::cxx_course::Dummy& that) : value{that.value} {
    std::cout << "Dummy(const Dummy&, value=" << value << ") @ " << this << std::endl;
}

se::ribomation::cxx_course::Dummy::~Dummy() {
    using namespace std;
    cout << "~Dummy() value=" << value << " @ " << this << endl;
}


