#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;
using namespace std::literals;

template<typename T>
T maximum(T a, T b) {
    return a > b ? a : b;
}

template<typename T>
T maximum(T a, T b, T c) {
    return maximum(a, maximum(b, c));
}


int main(int argc, char** argv) {
    if (argc != 4) {
        throw invalid_argument{"usage: "s + argv[0] + " <a> <b> <c>"};
    }

    auto a = stoi(argv[1]);
    auto b = stoi(argv[2]);
    auto c = stoi(argv[3]);
    cout << "maximum(" << a << "," << b << "," << c << ") = " << maximum(a, b, c) << endl;

    cout << "maximum(oh,no,blue) = " << maximum("oh"s, "no"s, "blue"s) << endl;

    return 0;
}


