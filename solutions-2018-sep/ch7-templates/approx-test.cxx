#include <iostream>
#include <iomanip>
#include <cmath>
#include "approx.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::numeric;


int main() {
    auto x = 4.5;
    auto n = 6U;

    auto resultApprox = sinApprox(x, n);
    auto result       = sin(x);

    cout << "sin(" << x << ")\n";
    cout << "  approx: " << fixed << resultApprox << " (" << n << " terms)\n";
    cout << " mathlib: " << fixed << result << "\n";
    cout << " absdiff: " << fixed << setprecision(20) << abs(result - resultApprox) << "\n";

    return 0;
}


