cmake_minimum_required(VERSION 3.10)
project(ch7_templates)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors -Wno-unused-parameter)

add_executable(maximum maximum.cxx)

add_executable(queue.hxx
        queue.hxx
        queue-app.cxx)

add_executable(maclaurin
        approx.hxx
        approx-test.cxx
        )
target_link_libraries(maclaurin m)

add_executable(sine-curve
        approx.hxx
        sine-curve.cxx
        )

