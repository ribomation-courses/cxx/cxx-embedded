#include <iostream>
#include <functional>
using namespace std;

inline auto reduce(int* arr, unsigned n, function<int(int,int)> f) {
    int       result = arr[0];
    for (auto k = 1U; k < n; ++k) result = f(result, arr[k]);
    return result;
}

int main() {
    int      numbers[] = {8, 2, 7, 4, 10, 9, 3, 1, 6, 5};
    unsigned N         = sizeof(numbers) / sizeof(numbers[0]);

    cout << "SUM : " << reduce(numbers, N, [](auto acc, auto k) {
        return acc + k;
    }) << endl;
    cout << "PROD: " << reduce(numbers, N, [](auto acc, auto k) {
        return acc * k;
    }) << endl;
    cout << "MAX : " << reduce(numbers, N, [](auto acc, auto k) {
        return acc < k ? k : acc;
    }) << endl;

    return 0;
}
