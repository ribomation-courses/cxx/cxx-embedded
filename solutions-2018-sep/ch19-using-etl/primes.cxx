#include <iostream>
#include "ETL/vector.h"

using namespace std;
using namespace std::literals;

void dump(etl::vector<unsigned, 100>& numbers) {
    cout << "[";
    for (auto p = 0U; p < numbers.size(); ++p) cout << numbers[p] << " ";
    cout << "]\n";
}

int main() {
    constexpr unsigned N = 1000;
    etl::vector<unsigned, N> numbers;
    for (auto p = 0U; p < N; ++p) numbers.push_back(1);

    for (auto p = 2U; p < numbers.size(); ++p) {
        while (numbers[p] == 0) ++p;
        for (auto k = 2 * p; k < numbers.size(); k += p) numbers[k] = 0;
    }

    for (auto p = 2U; p < numbers.size(); ++p) {
        if (numbers[p] == 1) cout << p << " ";
    }
    cout << endl;

    return 0;
}
