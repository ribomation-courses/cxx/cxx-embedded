#pragma once

#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <stdexcept>

using namespace std;

class Account {
    int   balance = 0;
    float rate    = 0.75;

    constexpr static unsigned capacity = 10;
    static vector<Account>    storage;
    static vector<bool>       used;

public:
    Account(int b, float r) : balance{b}, rate{r} {}

    ~Account() {
        cout << "~Account() @ " << this << endl;
    }

    Account() = default;
    Account(const Account&) = default;
    Account& operator =(const Account&) = default;

    string toString() const;

    friend ostream& operator <<(ostream& os, const Account& acc) {
        return os << acc.toString();
    }

    void* operator new(size_t);
    void operator delete(void* ptr);
};

