#include "account.hxx"

vector<Account>    Account::storage(Account::capacity);
vector<bool>       Account::used(Account::capacity);

void* Account::operator new(size_t) {
    cout << "Account::operator new()\n";
    auto idx = find_if(used.begin(), used.end(), [](auto x) { return !x; });
    if (idx == used.end()) {
        throw overflow_error{"no more slots"};
    }
    *idx = true;
    auto p = distance(idx, used.begin());
    return &storage[p];
}

void Account::operator delete(void* ptr) {
    cout << "Account::operator delete()\n";
    Account* PTR = reinterpret_cast<Account*>(ptr);
    auto idx = find_if(storage.begin(), storage.end(), [=](auto& x) { return PTR == &x; });
    if (idx == storage.end()) {
        throw overflow_error{"cannot find the mem blk"};
    }
    auto p = distance(idx, storage.begin());
    used[p] = false;
}

string Account::toString() const {
    ostringstream buf;
    buf << "Account{SEK " << balance << ", " << rate << "%} @ " << this;
    return buf.str();
}

