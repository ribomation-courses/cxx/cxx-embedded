#include <iostream>
#include "account.hxx"

using namespace std;
using namespace std::literals;

int main() {
    auto ptr = new Account{100,0.5};
    cout << "ptr: " << *ptr << endl;
    delete ptr;
    cout << "----\n";
    return 0;
}
