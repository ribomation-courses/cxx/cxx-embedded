#include <iostream>
#include <string>
#include <random>
#include <vector>
#include "Shape.hxx"
#include "Rect.hxx"
#include "Circle.hxx"
#include "Triangle.hxx"
#include "Square.hxx"
using namespace std;


Shape* mkShape() {
    static default_random_engine  r;
    uniform_int_distribution<int> nextShape{1, 4};
    uniform_int_distribution<int> nextValue{1, 10};
    switch (nextShape(r)) {
        case 1:
            return new Rect{nextValue(r), nextValue(r)};
        case 2:
            return new Circle{nextValue(r)};
        case 3:
            return new Triangle{nextValue(r), nextValue(r)};
        case 4:
            return new Square{nextValue(r)};
    }
    throw domain_error("WTF: this should not happen");
}

vector<Shape*> mkShapes(int n) {
    vector<Shape*> shapes;
    for (int k = 0; k < n; ++k) {
        shapes.push_back(mkShape());
    }
    return shapes;
}

int main(int numArgs, char* args[]) {
    int N = (numArgs <= 1) ? 4 : stoi(args[1]);

    auto shapes = mkShapes(N);

    cout << "---------------------\n";
    for (auto s : shapes) {
        cout << s << endl;
    }

    cout << "---------------------\n";
    for (auto s : shapes) { delete s; }

    return 0;
}
