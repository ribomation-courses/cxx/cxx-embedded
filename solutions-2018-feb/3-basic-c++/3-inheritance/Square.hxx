#pragma once

#include "Rect.hxx"


class Square : public Rect {
public:
    Square(int side)
            : Rect("square", side, side) { }

    ~Square() {
        cout << "~Square() " << this << endl;
    }
};
