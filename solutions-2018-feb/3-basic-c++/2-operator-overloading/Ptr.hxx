#pragma once

#include <stdexcept>
using namespace std;

template<typename T>
class Ptr {
    T* address = nullptr;

public:
    Ptr(T* address) : address{address} {}

    ~Ptr() {
        if (address) {
            delete address;
        }
    }

    Ptr() = default;
    Ptr(const Ptr<T>&) = delete;

    Ptr(Ptr<T>&& that) : address(that.address) {
        that.address = nullptr;
    }

    Ptr<T>& operator=(const Ptr<T>& that) {
        if (this != &that) {
            if (address) {
                delete address;
            }
            address = that.address;
            const_cast<Ptr<T>&>(that).address = nullptr;
        }
        return *this;
    }

    Ptr<T>& operator=(Ptr<T>&& that) {
        if (this != &that) {
            if (address) {
                delete address;
            }
            address = that.address;
            that.address = nullptr;
        }
        return *this;
    }

    T* operator->() {
        if (address) {
            return address;
        }
        throw invalid_argument("nullptr");
    }

    T& operator *() {
        if (address) {
            return *address;
        }
        throw invalid_argument("nullptr");
    }

};


