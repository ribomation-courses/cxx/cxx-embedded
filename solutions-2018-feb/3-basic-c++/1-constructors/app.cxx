#include <string>
#include <vector>
#include "TinyString.hxx"

using namespace std;
using namespace ribomation;

const char*  names[] = {"anna", "berit", "carin", "diana", "eva", "frida"};
const auto   names_size = sizeof(names) / sizeof(names[0]);

TinyString mk() {
    static int next = 0;
    cout << "mk(): " << names[next % names_size] << endl;
    return {names[next++ % names_size]};
}

int main() {
    TinyString s1{"Tjabba Habba"};
    cout << "s1: " << s1 << endl;

    cout << "---- (1) ----\n";
    s1 = mk();
    cout << "s1: " << s1 << endl;

    cout << "---- (2) ----\n";
    {
        TinyString s2 = move(s1);
        cout << "s1: " << s1 << endl;
        cout << "s2: " << s2 << endl;

        cout << "---- (2b) ----\n";
        TinyString s3 = s2 + " -- " + mk();
        cout << "s3: " << s3 << endl;
    }

    cout << "---- (3) ----\n";
    {
        TinyString s4{mk()};
        cout << "s4: " << s4 << endl;

        TinyString s5 = mk();
        cout << "s4: " << s5 << endl;
    }

    cout << "---- (4) ----\n";
    {
        TinyString ts = "";
        for (auto k = 0U; k < names_size; ++k) {
            ts += (k==0 ? "" : ", ") + mk();
        }
        cout << "ts: " << ts << endl;
    }

    //TinyString s4{s3};
    //error: use of deleted function ‘TinyString::TinyString(const TinyString&)’

    //s1 = s3;
    //error: use of deleted function ‘TinyString& TinyString::operator=(const TinyString&)’

    //TinyString s4;
    //error: use of deleted function ‘TinyString::TinyString()’

    cout << "---- (4)\n";
    return 0;
}
