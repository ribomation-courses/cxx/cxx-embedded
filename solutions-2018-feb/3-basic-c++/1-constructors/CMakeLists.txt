cmake_minimum_required(VERSION 3.9)
project(1_constructors)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra  -Werror -Wfatal-errors ")

add_executable(tiny-string-app
        TinyString.hxx
        app.cxx
        )
