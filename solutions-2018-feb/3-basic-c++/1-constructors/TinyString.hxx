#pragma once

#include <iostream>
#include <cstring>

using namespace std;

namespace ribomation {

    class TinyString {
        using Address = unsigned long;
        const char* payload = nullptr;
        bool        hasPayload()     const { return payload != nullptr; }
        Address     payloadAddress() const { return reinterpret_cast<Address>(payload); }
        char*       clone(const char* s) {
            return strcpy(new char[strlen(s) + 1], s);
        }

    public:
        TinyString()                             = delete;
        TinyString(const TinyString&)            = delete;
        TinyString& operator=(const TinyString&) = delete;

        TinyString(const char* s) : payload{clone(s)} {
            cout << "TinyString(char* '" << payload << "') @ " << this << endl;
        }

        ~TinyString() {
            cout << "~TinyString() @ " << this;
            if (hasPayload()) {
                cout << " DELETE \"" << payload << "\" @ " << payloadAddress();
                delete [] payload;
            } else {
                cout << " no payload";
            }
            cout << endl;
        }

        TinyString(TinyString&& that) : payload{that.payload} {
            that.payload = nullptr;
            cout << "TinyString(&& '" << payload << "') @ " << this << endl;
        }

        TinyString& operator=(TinyString&& that) {
            cout << "TinyString::operator=(&& '" << payload << "') @ " << this;
            if (this != &that) {
                if (hasPayload()) {
                    cout << " DELETE \"" << payload << "\"";
                    delete [] payload;
                }
                payload = that.payload;
                that.payload = nullptr;
            }
            cout << endl;
            return *this;
        }


        TinyString& operator +=(const TinyString& rhs) {
            const size_t  BUF_SIZE = 1 + strlen(this->payload) + strlen(rhs.payload);
            char          buf[BUF_SIZE];
            strcpy(buf, this->payload);
            strcat(buf, rhs.payload);

            delete [] this->payload;
            this->payload = clone(buf);

            return *this;
        }

        friend TinyString operator +(const TinyString& lhs, const TinyString& rhs) {
            const size_t  BUF_SIZE = 1 + strlen(lhs.payload) + strlen(rhs.payload);
            char          buf[BUF_SIZE];
            strcpy(buf, lhs.payload);
            strcat(buf, rhs.payload);
            return {buf};
        }

        friend std::ostream& operator <<(std::ostream& os, const TinyString& s) {
            return os << '"' << (s.hasPayload() ? s.payload : "") << "\" @ " << s.payloadAddress();
        }
    };

}
