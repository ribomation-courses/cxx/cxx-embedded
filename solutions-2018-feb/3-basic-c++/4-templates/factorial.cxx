#include <iostream>

using namespace std;

using UL = unsigned long;

template<unsigned N>
struct Factorial {
    constexpr static unsigned arg   = N;
    constexpr static UL       value = N * Factorial<N - 1>::value;
};

template<>
struct Factorial<1> {
    constexpr static unsigned arg   = 1;
    constexpr static UL       value = 1;
};



int main(int, char**) {
    struct Result {
        unsigned argument;
        UL       result;
    };

    Result results[] = {
            {Factorial<1>::arg,  Factorial<1>::value},
            {Factorial<2>::arg,  Factorial<2>::value},
            {Factorial<5>::arg,  Factorial<5>::value},
            {Factorial<10>::arg, Factorial<10>::value},
            {Factorial<15>::arg, Factorial<15>::value},
            {Factorial<20>::arg, Factorial<20>::value},
            {Factorial<25>::arg, Factorial<25>::value},
    };

    for (auto r : results)
        cout << r.argument << ": " << r.result << endl;
    return 0;
}


