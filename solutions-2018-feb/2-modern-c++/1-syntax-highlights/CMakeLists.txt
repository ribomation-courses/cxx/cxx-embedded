cmake_minimum_required(VERSION 3.9)
project(1_syntax_highlights)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic -Werror -Wfatal-errors")

add_executable(auto-foreach
        auto-foreach.cxx)

