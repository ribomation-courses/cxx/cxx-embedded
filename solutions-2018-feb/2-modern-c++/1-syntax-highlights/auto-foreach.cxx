#include <iostream>
#include <string>
#include <set>

using namespace std;
using namespace string_literals;

int main(int, char**) {
    //const char *
    auto words = set<string>{"super cool"s, "C++"s, "is"s, " Modern"s};
    for (auto& w : words) cout << w << " ";
    cout << endl;
    return 0;
}
