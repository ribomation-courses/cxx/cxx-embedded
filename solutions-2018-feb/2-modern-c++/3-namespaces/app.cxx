#include <iostream>
#include "dummy.hxx"
using namespace std;
using namespace se::ribomation::cxx_course;

void func1(const Dummy& x) {
    cout << "[func-1] &x = " << &x << endl;
}

void func2(Dummy x) {
    cout << "[func-2] &x = " << &x << endl;
}

int main() {
    Dummy d{};
    func1(d);
    func2(d);
    return 0;
}