#pragma once

namespace se {
    namespace ribomation {
        namespace cxx_course {

            struct Dummy {
                Dummy();
                Dummy(const Dummy&);
                virtual ~Dummy();
            };

        }
    }
}

