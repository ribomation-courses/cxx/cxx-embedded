#include <iostream>
#include "dummy.hxx"


se::ribomation::cxx_course::Dummy::Dummy() {
    using std::cout;
    using std::endl;
    cout << "Good-day from a Dummy class @ " << this << endl;
}

se::ribomation::cxx_course::Dummy::Dummy(const se::ribomation::cxx_course::Dummy&) {
    std::cout << "Copy of a Dummy class @ " << this << std::endl;
}

se::ribomation::cxx_course::Dummy::~Dummy() {
    using namespace std;
    cout << "Good-bye from a Dummy class @ " << this << endl;
}
