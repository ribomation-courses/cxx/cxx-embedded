#include <iostream>
#include <functional>

using namespace std;

long reduce(short numbers[], size_t N, const function<long(long, short)>& f) {
    long      result = numbers[0];
    for (auto k      = 1U; k < N; ++k) {
        result = f(result, numbers[k]);
    }
    return result;
}

int main(int, char**) {
    short numbers[]    = {
            1, 1, 2, 3, 5, 8, 13, 21, 34, 55
    };
    const auto numbers_size = sizeof(numbers) / sizeof(numbers[0]);

    for (auto x:numbers) cout << x << " ";
    cout << endl;

    cout << "SUM    : " << reduce(numbers, numbers_size, [](auto acc, auto x){
        return acc + x;
    }) << endl;

    cout << "PRODUCT: " << reduce(numbers, numbers_size, [](auto acc, auto x){
        return acc * x;
    }) << endl;

    cout << "LARGEST: " << reduce(numbers, numbers_size, [](auto acc, auto x){
        return x > acc ? x : acc;
    }) << endl;

    return 0;
}

