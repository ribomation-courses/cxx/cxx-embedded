#include <iostream>

using namespace std;

int main() {
    unsigned numbers[] = {
            1, 1, 2, 3, 5, 8, 13, 21, 34, 55
    };
    const auto numbers_size = sizeof(numbers) / sizeof(numbers[0]);

    for (auto x:numbers) cout << x << " ";
    cout << endl;

    const auto C = 10;
    auto       f = [=](auto x) { return x + C; };

    for (auto  k = 0U; k < numbers_size; ++k) {
        numbers[k] = f(numbers[k]);
    }

    for (auto x:numbers) cout << x << " ";
    cout << endl;

    return 0;
}